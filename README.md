# README #

### To start backend: ###
cd back  
npm install  
npm run develop

admin panel address: http://localhost:1337/admin

admin creds:  
login: admin@mail.com  
pwd: SecretPassword1

moderator creds:  
login: editor@mail.com  
pwd: SecretPassword1

### To start frontend: ###

cd front  
npm install  
npm run start

frontend address: http://localhost:3000/
