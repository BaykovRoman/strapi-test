import {Image} from "./Image";

export type Author = {
    id: number,
    name: string,
    email: string,
    picture: Image,
}
