import {Image} from "./Image";
import {Author} from "./Author";
import {Category} from "./Category";

export type Article = {
    id: number,
    title: string,
    description: string,
    content: string,
    slug: string,
    category: Category,
    author: Author,
    published_at: string,
    created_at: string,
    updated_at: string,
    image: Image,
}
