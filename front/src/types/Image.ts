import {Picture} from "./Picture";

export type Image = Picture & {
    id: number,
    alternativeText: string | null,
    caption: string | null,
    formats: {
        thumbnail: Picture,
        large: Picture,
        medium: Picture,
        small: Picture,
    },
    previewUrl: string | null,
    privider: string | null,
    provider_metadata: string | null,
    created_at: string,
    updated_at: string,
}
