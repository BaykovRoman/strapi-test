export enum LoadStatus {
    Initial = 'initial',
    Pending = 'pending',
    Loaded = 'loaded',
    Error = 'error',
}
