import React, {useEffect} from "react"
import {useDispatch} from 'react-redux'
import {loadArticles} from "./store/reducers/strapiSlice";
import ArticlesListPage from "./components/ArticlesListPage";
import ArticlePage from "./components/ArticlePage";
import {BrowserRouter as Router, Route} from 'react-router-dom'
import "./App.css"
import {Box} from "@material-ui/core";

const App: React.FC = () => {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(loadArticles())
    })

    return (
        <Router>
            <Route path='/' render={(props) => (
                <Box m={5}>
                    <ArticlesListPage/>
                </Box>
            )} exact/>
            <Route path='/article/:slug' component={ArticlePage} exact/>
        </Router>
    );
}

export default App;
