import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import axios from "axios"
import {LoadStatus} from "../../enums/LoadStatus";
import {Article} from "../../types/Article";

export interface IState {
    articles: Array<Article>,
    status: LoadStatus
}

const initialState: IState = {
    articles: [],
    status: LoadStatus.Initial
}

export const loadArticles = createAsyncThunk(
    'strapi/loadArticles',
    async () => {
        const results = await axios.get(`http://localhost:1337/articles`)
        return results.data;
    }
);

export const strapiSlice = createSlice({
    name: 'strapi',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(loadArticles.pending, (state) => {
                state.status = LoadStatus.Pending
            })
            .addCase(loadArticles.fulfilled, (state, action) => {
                console.log('loaded articles')
                state.articles = action.payload;
                state.status = LoadStatus.Loaded
            })
            .addCase(loadArticles.rejected, (state) => {
                state.status = LoadStatus.Error;
            })
    },
});

export default strapiSlice.reducer;
