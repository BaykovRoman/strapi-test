import React from "react";
import {
    Box,
    CircularProgress
} from "@material-ui/core";

const FullScreenLoader: React.FC = () => {
    return (
        <Box height={"100%"} display="flex" flexDirection="column" justifyContent="center">
            <Box mx="auto"><CircularProgress/></Box>
        </Box>
    )
}

export default FullScreenLoader
