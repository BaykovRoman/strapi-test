import React from "react"
import {Article} from "../types/Article";
import {
    Box, Paper, Typography
} from "@material-ui/core";
import {useHistory} from "react-router";

type TProps = {
    article: Article
}

const ArticleCard: React.FC<TProps> = ({article}) => {
    const history = useHistory()

    return (
        <Box height={"100%"} className={"pointer"}
             onClick={() => {
                 history.push({pathname: `/article/${article.slug}`})
             }}
        >
            <Paper className={"h100"} elevation={3}>
                <Box minHeight={"300px"} height={"100%"} display={"flex"} flexDirection={"column"}>
                    <Box height={"300px"} flexGrow={1} style={{
                        backgroundImage: `url("http://localhost:1337${article.image.formats.small.url}")`,
                        backgroundSize: "cover",
                    }}/>
                    <Box m={3} flexGrow={1}>
                        <Typography variant="h3" align={"center"}>
                            {article.title}
                        </Typography>
                    </Box>
                </Box>
            </Paper>
        </Box>
    )
}

export default ArticleCard
