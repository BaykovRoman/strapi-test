import React, {useEffect} from "react"
import {RouteComponentProps} from "react-router-dom";
import axios from "axios";
import {LoadStatus} from "../enums/LoadStatus";
import {
    Box,
    Paper,
    Slide,
    Typography,
    Button,
} from "@material-ui/core";
import FullScreenLoader from "./FullScreenProgress";
import {Article} from "../types/Article";
import {useHistory} from "react-router";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import {decode} from "html-entities";

const removeMd = require('remove-markdown');

type TParams = {
    slug: string | undefined
}

const ArticlePage: React.FC<RouteComponentProps<TParams>> = ({match}: RouteComponentProps<TParams>) => {

    const [status, setStatus] = React.useState(LoadStatus.Initial);
    const [article, setArticle] = React.useState<Article>();

    const history = useHistory()

    useEffect(() => {
        const loadArticle = async () => {
            const articleData = await axios.get(`http://localhost:1337/articles?slug=${match.params.slug}`)
            setArticle(articleData.data[0])
            setStatus(LoadStatus.Loaded)
        }
        setStatus(LoadStatus.Pending)
        try {
            loadArticle()
        } catch (error) {
            setStatus(LoadStatus.Error)
        }
    }, [match.params.slug])

    return (<>
        {status === LoadStatus.Loaded && article !== undefined ? (
            <>
                <Box className={"pointer"}
                    onClick={() => {history.push({pathname: `/`})}}
                >

                    <Button
                        variant="contained"
                        color="secondary"
                        startIcon={<ArrowBackIcon className="colorBlue"/>}
                    >
                        Back
                    </Button>
                </Box>
                <Slide direction="left" in={true}>
                    <Box>
                        <Paper className={"h100"} elevation={3}>
                            <Box minHeight={"600px"} height={"100%"} display={"flex"} flexDirection={"column"}>
                                <Box height={"500px"} flexGrow={1} style={{
                                    backgroundImage: `url("http://localhost:1337${article.image.formats.small.url}")`,
                                    backgroundSize: "cover",
                                    backgroundPosition: "50% 50%",
                                }}/>
                                <Box m={3}>
                                    <Typography variant="h2" align={"center"}>
                                        {article.title}
                                    </Typography>
                                </Box>
                                <Box m={3}>
                                    <Typography variant="body1" align={"justify"}>
                                        {removeMd(decode(article.content))}
                                    </Typography>
                                </Box>
                            </Box>
                        </Paper>
                    </Box>
                </Slide>
                <Box className={"pointer"}
                     onClick={() => {history.push({pathname: `/`})}}
                >

                    <Button
                        variant="contained"
                        color="secondary"
                        startIcon={<ArrowBackIcon className="colorBlue"/>}
                    >
                        Back
                    </Button>
                </Box>
            </>
        ) : <FullScreenLoader/>}
    </>)
}

export default ArticlePage
