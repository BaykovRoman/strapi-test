import React from "react"
import {LoadStatus} from "../enums/LoadStatus"
import {useSelector} from "react-redux"
import {RootState} from "../store/store"
import {
    Slide,
    Grid,
} from "@material-ui/core"
import FullScreenLoader from "./FullScreenProgress";
import ArticleCard from "./ArticleCard";

const ArticlesListPage: React.FC = () => {

    const status = useSelector((state: RootState) => state.strapi.status)
    const articles = useSelector((state: RootState) => state.strapi.articles)

    return (
        <>
            {status === LoadStatus.Loaded ? (
                <Slide direction="right" in={true}>
                    <Grid container spacing={3}>
                        {
                            Object.keys(articles.slice(0, 4)).map((key, index) => {
                                    const article = articles.slice(0, 4)[index]
                                    return (
                                        <Grid key={key} item xs={12} sm={6} md={3}>
                                            <ArticleCard article={article}/>
                                        </Grid>
                                    )
                                }
                            )
                        }
                    </Grid>
                </Slide>
            ) : <FullScreenLoader/>}
        </>
    )
}

export default ArticlesListPage
